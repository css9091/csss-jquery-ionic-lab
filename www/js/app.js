$(document).ready(function(){

	$(function() {
		// Show default wechat tab content.
        $('.person').click(function() {
            
            var name = $(this).children().attr('src');
            name = name.substring(4, name.length - 4);
            
            $(this).append(name);
        });
        $(".person").trigger('click');
        $(".person").unbind('click');
        
        showTab('wechat');

		bindNaviTabEvents();
	});
});

function showTab (tabId) {
	// Hide all content tab.
	$('.content').hide();

	// Show the specified content.
	$('#' + tabId + '-content').show();
}

function bindNaviTabEvents () {
	
	$('.icon').click(function () {

		var tabId = $(this).attr('id');

		showTab(tabId);
	});
}